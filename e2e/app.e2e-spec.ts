import { LoianeFormsPage } from './app.po';

describe('loiane-forms App', () => {
  let page: LoianeFormsPage;

  beforeEach(() => {
    page = new LoianeFormsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
