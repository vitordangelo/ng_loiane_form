import { Component, OnInit } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import 'rxjs/add/operator/map'; 

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit {

  usuario: any = {
    nome: "Vitor",
    email: "vitor@inatl.br"
  }

  constructor(private http: Http) { }

  ngOnInit() {
  }

  onSubmit(form) {
    // console.log(form);
    // console.log(this.usuario);

    let formJson = JSON.stringify(form.value);
    this.http.post('https://httpbin.org/post', formJson)
      .map(res => res)
      .subscribe(dados => console.log(dados))
  }

  consultaCEP(cep, form) {
    cep = cep.replace(/\D/g, '');
    if (cep != "") {
      var validaCEP = /^[0-9]{8}$/;
      if (validaCEP.test(cep)) {
        console.log(cep);
        this.resetaDadosForm(form);
        
        this.http.get(`//viacep.com.br/ws/${cep}/json`)
          .map(dados => dados.json())
          .subscribe(cep => {
            this.populaDadosForm(cep, form)
          });
      }
    }
  }

  populaDadosForm(dados, form) {
    // form.setValue({
    //   nome: form.value.nome,
    //   email: form.value.email,
    //     endereco: {
    //       rua: dados.logradouro,
    //       cep: dados.cep,
    //       bairro: dados.bairro,
    //       cidade: dados.localidade,
    //       estado: dados.uf,
    //       numero: '',
    //       complemento: dados.complemento
    //     }
    // });

    form.form.patchValue({
      endereco: {
      rua: dados.logradouro,
      cep: dados.cep,
      bairro: dados.bairro,
      cidade: dados.localidade,
      estado: dados.uf,
      numero: '',
      complemento: dados.complemento
        }
    })
  }

  resetaDadosForm(form) {
    form.form.patchValue({
      endereco: {
        rua: null,
        cep: null,
        bairro: null,
        cidade: null,
        estado: null,
        numero: null,
        complemento: null
      }
    });
  }

}
