import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FormDebugComponent } from './../form-debug/form-debug.component';
import { TemplateFormComponent } from "./template-form.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    FormDebugComponent,
    TemplateFormComponent
  ]
})
export class TemplateFormModule { }
